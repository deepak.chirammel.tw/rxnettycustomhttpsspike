 # Pre-Requisites
 Java (JDK 8)

 IntelliJ (IDE - Community Edition)

 ## Running via command-line (cmd Terminal):
 `gradlew run`

 ## Running via Bash Terminal:
 `./gradlew run`

 ## Running from Editor (from IntelliJ):
 Right click and run `src/main/java/com/tw/arcast/server/RxNettyCustomHttpsServer.java` for Secured Https connections using `unsafeSecured()` option.

 ## Notes
 The `libs` folder contains the custom build (modified) OpenSource RxNetty library jars (for `rxnetty-common`, `rxnetty-http` and `rxnetty-tcp` library jars).
