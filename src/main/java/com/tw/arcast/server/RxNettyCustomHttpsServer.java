package com.tw.arcast.server;

import com.tw.arcast.ExamplesEnvironment;
import io.reactivex.netty.protocol.http.server.HttpServer;
import rx.Observable;

public final class RxNettyCustomHttpsServer {

    public static void main(String[] args) {

        ExamplesEnvironment env = ExamplesEnvironment.newEnvironment(RxNettyCustomHttpsServer.class);

        HttpServer server = HttpServer.newServer().unsafeSecure()//Enable HTTPS for demo purpose only, for real apps, use secure() methods instead.
                .start((req, resp) ->
                        resp.writeString(Observable.just("Hello World!"))
                );
        System.out.println("Server Started at: https://localhost:" + server.getServerPort());

        /*Wait for shutdown if not called from the client (passed an arg)*/
        if (env.shouldWaitForShutdown(args)) {
            server.awaitShutdown();
        }

        /*If not waiting for shutdown, assign the ephemeral port used to a field so that it can be read and used by
        the caller, if any.*/
        env.registerServerAddress(server.getServerAddress());

    }

}
